# README #

This repository is a place for "great" ideas.

#
##
###
#### StatHub
###
##
#

*
** High Level
*
Environment for users to upload and download datasets.

*
** Low Level
*
Users will pay a fee for one month of access, up to X gigabytes of downloaded data.
Users will be able to upload and download data by means of an API.
Datasets can be augmented with user-generated analysis.
Users will be able to participate in a forum.
Users are incentivized to upload data.

*
** Stuff to figure out
*
How should users be incentivized to upload data?
	Through extended or cheaper access?
	User forum "experience ratings"?
How can we verify that datasets are correct? Or how can we verify that a database is not a duplicate?
	Some incentivization mechanism (game theory)

#
##
###
#### Blockchain based gambling
###
##
#

*
** High Level
*
ummm....


